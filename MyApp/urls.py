from django.urls import include, path
from django.views.generic.base import TemplateView
from django.contrib.auth import views as auth_views
from . import views
urlpatterns = [
    path('accounts/', include('django.contrib.auth.urls')),
    path('', TemplateView.as_view(template_name='home1.html'), name='home1'),
    path('home/', views.Homepage, name='home'),
    path('social-auth/', include('social_django.urls', namespace='social')),
    path('signup/', views.signupform, name='signup'),
    path('login/', views.Userlogin, name='login'),
    path('logout/', views.UserLogout, name='logout'),
    path('contact/', views.contact, name="contact"),
    path('about/', views.about, name="about"),
    path('password_reset/$', auth_views.PasswordResetView.as_view(), name='password_reset'),
    path('password_reset/done/$', auth_views.PasswordResetDoneView.as_view(), name='password_reset_done'),
    path('password-reset-confirm/<uidb64>/<token>/', auth_views.PasswordResetConfirmView.as_view(),
         name='password_reset_confirm'),
    path('reset/done/$', auth_views.PasswordResetCompleteView.as_view(), name='password_reset_complete'),

]